package com.atlassian.confluence.ext.code;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.ext.code.render.ContentFormatter;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.SubRenderer;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.macro.basic.AbstractPanelMacro;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * A new code macro which relies on the SyntaxHighlighter JavaScript library to
 * do the actual highlighting client-side.
 */
public final class NewCodeMacro extends AbstractPanelMacro implements Macro {

    private static final Logger LOG = LoggerFactory.getLogger(NewCodeMacro.class);

    private final ContentFormatter contentFormatter;

    private static boolean pdlEnabled = Long.parseLong(GeneralUtil.getBuildNumber()) >= 4000;

    /**
     * Default constructor.
     */
    public NewCodeMacro(final ContentFormatter contentFormatter,
                        final @ComponentImport SubRenderer subRenderer) {
        this.setSubRenderer(subRenderer);
        this.contentFormatter = contentFormatter;
    }

    /**
     * @see com.atlassian.renderer.v2.macro.basic.AbstractPanelMacro#getPanelCSSClass()
     */
    @Override
    protected String getPanelCSSClass() {
        return pdlEnabled ? "code panel pdl" : "code panel";
    }

    /**
     * @see com.atlassian.renderer.v2.macro.basic.AbstractPanelMacro#getPanelHeaderCSSClass()
     */
    @Override
    protected String getPanelHeaderCSSClass() {
        return pdlEnabled ? "codeHeader panelHeader pdl" : "codeHeader panelHeader";
    }

    /**
     * @see com.atlassian.renderer.v2.macro.basic.AbstractPanelMacro#getPanelContentCSSClass()
     */
    @Override
    protected String getPanelContentCSSClass() {
        return pdlEnabled ? "codeContent panelContent pdl" : "codeContent panelContent";
    }

    /**
     * @see com.atlassian.renderer.v2.macro.basic.AbstractPanelMacro#getBodyRenderMode()
     */
    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    /**
     * @see com.atlassian.renderer.v2.macro.BaseMacro#suppressMacroRenderingDuringWysiwyg()
     */
    @Override
    public boolean suppressMacroRenderingDuringWysiwyg() {
        return false;
    }

    /**
     * Writes the actual content of this macro.
     * <ol>
     * <li>Include needed resources such as the script files and CSS files</li>
     * <li>Create the &lt;pre&gt; tag to hold the content which will be rendered
     * by the SyntaxHighlighter</li>
     * <li>Parse any parameters into something the SyntaxHighlighter can
     * understand</li>
     * </ol>
     *
     * @param parameters    The parameters entered by the user.
     * @param body          The body of the macro to render.
     * @param renderContext The render context.
     * @return The rendered content.
     * @throws MacroException In case of rendering errors.
     */
    @SuppressWarnings("unchecked")
    @Override
    public String execute(final Map parameters, final String body, final RenderContext renderContext) throws MacroException {
        try {
            return execute(parameters, body, new DefaultConversionContext(renderContext));
        } catch (MacroExecutionException e) {
            throw new MacroException(e);
        }
    }

    @Override
    public String execute(final Map<String, String> parameters,
                          String body,
                          final ConversionContext conversionContext) throws MacroExecutionException {
        LOG.debug("Executing newcode macro for body: {}", body);

        // Since the 3.5.x implementation of this macro delegates to this method, where we now
        // perform our own html encoding of the macro body, we don't need the V2 renderer doing
        // it for us anymore (which was invoked by overriding getBodyRenderMode() to return
        // F_HTMLESCAPE)
        // This way, we avoid undesirable double html encoding (NCODE-160) of the macro body.
        //
        // CONFDEV-4893 - GeneralUtil.htmlEncode encodes ' to &#39; which while not wrong is
        // unnecessary. The javascript used by the new code macro doesn't handle numeric entities
        // properly so we use a more correct html encoding utility here instead.

        // NCODE-172 StringEscapeUtils.escapeHtml will cause multi-byte characters problem.
        // We have to use GeneralUtil.htmlEncode here but we need to replace &#39; in the
        // javascript manually
        body = GeneralUtil.htmlEncode(body);

        String formatted;
        try {
            formatted = this.contentFormatter.formatContent(conversionContext, parameters, body);
            Map<String, String> panelParameters = contentFormatter.getPanelParametersWithThemeLayout(parameters);
            String content = super.execute(panelParameters, formatted, conversionContext.getPageContext());

            boolean collapse = StringUtils.isNotBlank(parameters.get("collapse")) &&
                    Boolean.parseBoolean(parameters.get("collapse"));
            if (collapse) {
                content = addExpandCollapseHtml(content);
            }

            LOG.debug("Newcode macro execution finished, resulting content: {}", content);

            return content;
        } catch (MacroException e) {
            throw new MacroExecutionException(e);
        } catch (Exception e) {
            return getText("newcode.render.invalid.parameter", e) + "<pre>" + body + "</pre>";     // NCODE-216
        }
    }

    /**
     * Return the i18n-ed message based on the key and the parameters.
     *
     * @param key    The message key
     * @param params The message parameters
     * @return The error message
     */
    private String getText(final String key, final Object... params) {
        return ConfluenceActionSupport.getTextStatic(key, params);
    }

    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

    public TokenType getTokenType(Map map, String body, RenderContext renderContext) {
        return TokenType.BLOCK;
    }

    protected void setPdlEnabled(boolean pdlEnabled) {
        this.pdlEnabled = pdlEnabled;
    }

    private String addExpandCollapseHtml(String content) {
        final Document doc = Jsoup.parse(content);
        final Elements headerElements = doc.select("div.codeHeader");

        if (headerElements != null && StringUtils.isNotEmpty(headerElements.toString())) {
            final Element headerElement = headerElements.get(0);
            headerElement.addClass("hide-border-bottom");
            headerElement.child(0).addClass("code-title");
            headerElement.child(0).after(addCollapseSourceHtml());

            final Element contentElement = doc.select("div.codeContent").get(0);
            contentElement.addClass("hide-toolbar");
        } else {
            final Element contentElement = doc.select("div.codeContent").get(0);
            contentElement.before(addHeaderHtml());
            contentElement.addClass("hide-toolbar");
        }

        return doc.body().html();
    }

    private String addHeaderHtml() {
        return "<div class=\"" + getPanelHeaderCSSClass() + " hide-border-bottom" + "\">" +
                "<b class='code-title'></b>" +
                addCollapseSourceHtml() +
                "</div>";
    }

    private String addCollapseSourceHtml() {
        return "<span class='collapse-source expand-control' style='display:none;'>" +
                "<span class='expand-control-icon icon'>&nbsp;</span>" +
                "<span class='expand-control-text'>" +
                getText("newcode.config.expand.source") +
                "</span></span>" +
                "<span class='collapse-spinner-wrapper'></span>";
    }

}
