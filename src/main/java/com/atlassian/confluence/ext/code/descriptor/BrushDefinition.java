package com.atlassian.confluence.ext.code.descriptor;

/**
 * Value object for the definition of a brush in atlassian-plugin.xml.
 */
public final class BrushDefinition {

    private String location;
    private String webResourceId;

    /**
     * Default constructor.
     *
     * @param location      The location (in the classpath) of the brush file
     * @param webResourceId The Web Resource module to include for this brush
     */
    public BrushDefinition(final String location, final String webResourceId) {
        super();
        this.location = location;
        this.webResourceId = webResourceId;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @return the webResourceId
     */
    public String getWebResourceId() {
        return webResourceId;
    }

}
