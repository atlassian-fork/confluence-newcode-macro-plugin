package com.atlassian.confluence.ext.code.descriptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Facade for changing the Confluence descriptors. Delegates all calls to the
 * actual strategy compatible with the current version of Confluence.
 */
@Component
public final class DescriptorFacadeImpl implements DescriptorFacade {

    private static final Logger LOG = LoggerFactory.getLogger(DescriptorFacadeImpl.class);

    private final ConfluenceStrategy strategy;

    @Autowired
    public DescriptorFacadeImpl(final ConfluenceStrategy loadingStrategy) {
        this.strategy = loadingStrategy;
    }

    /**
     * {@inheritDoc}
     */
    public BrushDefinition[] listBuiltinBrushes() {
        LOG.debug("Retrieving declared brushes");
        BrushDefinition[] result = this.strategy.listBuiltinBrushes();

        if (LOG.isDebugEnabled()) {
            StringBuilder builder = new StringBuilder();
            builder.append("[");
            for (BrushDefinition brush : result) {
                builder.append(brush.getLocation()).append(',');
            }
            builder.append("]");
            LOG.debug("Declared brushes retrieved: {}", builder);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public ThemeDefinition[] listBuiltinThemes() {
        LOG.debug("Retrieving declared themes");
        ThemeDefinition[] result = this.strategy.listBuiltinThemes();

        if (LOG.isDebugEnabled()) {
            StringBuilder builder = new StringBuilder();
            builder.append("[");
            for (ThemeDefinition tmp : result) {
                builder.append(tmp.getLocation()).append(',');
            }
            builder.append("]");
            LOG.debug("Declared brushes themes: {}", builder);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public List<String> listLocalization() {
        LOG.debug("Retrieving declared localization");
        List<String> result = this.strategy.listLocalization();

        LOG.debug("Declared localizations: {}", result);
        return result;
    }
}
