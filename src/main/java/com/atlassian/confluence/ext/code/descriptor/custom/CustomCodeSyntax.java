package com.atlassian.confluence.ext.code.descriptor.custom;

/**
 * Contract interface for the codeSyntax plugin module.
 */
public interface CustomCodeSyntax {
    /**
     * Identifies the web resource plugin module containing the JavaScript brush file for the SyntaxHighlighter for this
     * custom language. This must be fully-qualified module key (eg. 'com.atlassian.plugin.myplugin:my-resource-key')
     *
     * @return The web resource plugin module key for this custom language/
     */
    String getResourceKey();

    /**
     * Gets the friendly name that will be used to describe this syntax highlighter to the user.
     */
    String getFriendlyName();
}
