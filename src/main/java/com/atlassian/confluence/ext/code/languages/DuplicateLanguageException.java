package com.atlassian.confluence.ext.code.languages;

/**
 * Exception which is thrown by the {@link LanguageRegistry} in case a language
 * is registered with a duplicate name or a duplicate alias.
 */
public final class DuplicateLanguageException extends Exception {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1L;

    private String languageNameInError;

    /**
     * Default constructor.
     */
    public DuplicateLanguageException(final String msg,
                                      final String languageNameInError) {
        super(msg);
        this.languageNameInError = languageNameInError;
    }

    /**
     * @return The already-registered language name that caused this exception to be thrown.
     */
    public String getLanguageNameInError() {
        return languageNameInError;
    }
}