package com.atlassian.confluence.ext.code.languages;

/**
 * Thrown in case of problems with the the language does not exist.
 */
public final class UnknownLanguageException extends Exception {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1L;

    private String alias;

    /**
     * Default constructor.
     *
     * @param alias The alias of the language
     */
    public UnknownLanguageException(final String alias) {
        super();
        this.alias = alias;
    }

    /**
     * @return the alias
     */
    public String getAlias() {
        return alias;
    }

}
