package com.atlassian.confluence.ext.code.render;

/**
 * Exception thrown in case of an invalid value of a parameter.
 */
public final class InvalidValueException extends Exception {

    /**
     * SerialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private String parameter;

    /**
     * Default constructor.
     *
     * @param parameter The parameter which has an invalid value
     */
    public InvalidValueException(final String parameter) {
        this.parameter = parameter;
    }

    /**
     * @return the parameter
     */
    public String getParameter() {
        return parameter;
    }

}
