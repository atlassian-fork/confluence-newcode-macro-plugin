package com.atlassian.confluence.ext.code.render;

import java.util.Map;

/**
 * Parameter implementation for the "first-line" parameter.
 */
public final class LineNumbersParameter extends AbstractBooleanMappedParameter {

    /**
     * Default constructor.
     *
     * @param name       The name used by the macro
     * @param mappedName The name used by the syntax highlighter
     */
    public LineNumbersParameter(final String name, final String mappedName) {
        super(name, mappedName);
    }

    /**
     * @see com.atlassian.confluence.ext.code.render.BooleanMappedParameter#getValue
     * (java.util.Map)
     */
    @Override
    public String getValue(final Map<String, String> parameters)
            throws InvalidValueException {
        String value = internalGetValue(parameters);
        // Default to false (which contradicts with the SyntaxHighlighter
        // library
        // to ensure conformance with the old code macro
        return (value == null) ? "false" : value;
    }

}
