package com.atlassian.confluence.ext.code.render;

import java.util.Map;

/**
 * Implementation of a parameter for which a mapping has to be made between the
 * name used by the macro and the syntax highlighter library.
 */
class MappedParameter implements Parameter {
    private String name;
    private String mappedName;

    /**
     * Default constructor.
     *
     * @param name       The name used by the macro
     * @param mappedName The name used by the syntax highlighter
     */
    MappedParameter(final String name, final String mappedName) {
        super();
        this.name = name;
        this.mappedName = mappedName;
    }

    /**
     * {@inheritDoc}
     */
    public final String getName() {
        return mappedName;
    }

    /**
     * {@inheritDoc}
     */
    public final String getMacroName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    public String getValue(final Map<String, String> parameters)
            throws InvalidValueException {
        return parameters.get(name);
    }

}
