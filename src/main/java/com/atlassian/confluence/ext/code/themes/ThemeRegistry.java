package com.atlassian.confluence.ext.code.themes;

import java.util.Collection;
import java.util.Map;

/**
 * Registry for themes supported by this plugin.
 */
public interface ThemeRegistry {

    /**
     * Returns whether a certain theme is know to the system.
     *
     * @param theme The theme name
     * @return Whether the theme is know to the system
     */
    boolean isThemeRegistered(String theme);

    /**
     * List all registered themes.
     *
     * @return the names of the registered themes
     */
    Collection<Theme> listThemes() throws Exception;

    /**
     * Returns the full web resource id for a theme.
     *
     * @param name The name of the theme
     * @return The web resource id to include
     * @throws UnknownThemeException In case of an unknown theme
     */
    String getWebResourceForTheme(String name) throws Exception;

    /**
     * Returns the look and feel for a theme.
     *
     * @param name The name of the theme
     * @return The look and feel for the theme
     * @throws UnknownThemeException In case of an unknown theme
     */
    Map<String, String> getThemeLookAndFeel(String name) throws Exception;
}
