package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.languages.DuplicateLanguageException;
import com.atlassian.confluence.ext.code.languages.InvalidLanguageException;
import com.atlassian.confluence.ext.code.languages.LanguageParser;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.atlassian.confluence.ext.code.languages.RegisteredLanguageInstaller;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginController;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Unit Tests for {@link RegisteredLanguageInstallerImpl}
 */
public class RegisteredLanguageInstallerTestCase extends TestCase {
    @Mock
    private LanguageRegistry languageRegistry;
    @Mock
    private LanguageParser languageParser;
    @Mock
    private PluginController pluginController;
    @Mock
    private PluginGenerator pluginGenerator;

    private RegisteredLanguageInstaller languageInstaller;
    private Reader testLanguageScript = new StringReader("This is the contents of a test brush file, really!");
    private String testFriendlyName = "This it the friendly name of the new language.";

    @Override
    protected void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        languageInstaller = new RegisteredLanguageInstallerImpl(languageRegistry, languageParser, pluginController, pluginGenerator);
    }

    /**
     * Tests the happy path of the language installer.
     */
    public void testInstallRegisteredLanguage() throws Exception {
        RegisteredLanguage mockLanguage = getMockLanguage();
        when(languageRegistry.isLanguageRegistered(anyString())).thenReturn(false);

        PluginArtifact mockPlugin = getMockPlugin(mockLanguage);

        languageInstaller.installLanguage(testLanguageScript, testFriendlyName);

        verify(pluginController).installPlugins(mockPlugin);
    }

    /**
     * Tests that no plugin is installed if the dynamically-generated plugin containing the new language fails to be
     * created for any reason.
     */
    public void testDynamicPluginGenerationFails() throws Exception {
        RegisteredLanguage mockLanguage = getMockLanguage();
        when(languageRegistry.isLanguageRegistered(anyString())).thenReturn(false);

        when(pluginGenerator.createPluginForLanguage(eq(mockLanguage), any(StringReader.class))).thenThrow(new IOException("the plugin couldn't be generated because of some I/O problem"));

        try {
            languageInstaller.installLanguage(testLanguageScript, testFriendlyName);
            fail("Expected an IOException by now");
        } catch (InvalidLanguageException e) {
            // Expected this.
            assertTrue(e.getCause() instanceof IOException);
        }

        // Nothing should have been installed - important!
        verifyZeroInteractions(pluginController);
    }

    /**
     * Tests that no plugin is generated and installed when the supplied brush for the code macro would register a
     * language name that is already installed in the code macro.
     */
    public void testInstallDuplicateLanguage() throws Exception {
        getMockLanguage();
        when(languageRegistry.isLanguageRegistered("name")).thenReturn(true);

        try {
            languageInstaller.installLanguage(testLanguageScript, "friendly name");
            // Expecting DuplicateLanguageException
            fail("duplicate language was not detected correctly.");
        } catch (DuplicateLanguageException e) {
            // all good!
        }

        // Nothing should have been installed - important!
        verifyZeroInteractions(pluginController);
    }

    /**
     * Tests that no plugin is generated and installed when the supplied brush for the code macro is not valid according
     * to the rules of the {@link LanguageParser}.
     */
    public void testInstallInvalidLanguage() throws Exception {
        when(languageParser.parseRegisteredLanguage(any(StringReader.class), anyString())).thenThrow(new InvalidLanguageException("invalid"));
        try {
            languageInstaller.installLanguage(new StringReader("test"), "friendly name");
            fail("invalid language script was not detected correctly.");
        } catch (InvalidLanguageException e) {
            // this is expected.
        }

        // Nothing should have been installed - important!
        verifyZeroInteractions(pluginController);
    }

    private RegisteredLanguage getMockLanguage() throws InvalidLanguageException {
        ArrayList<String> aliases = new ArrayList<String>();
        aliases.add("name");
        aliases.add("name2");
        RegisteredLanguage mockLanguage = new RegisteredLanguage("name", aliases, testFriendlyName);
        mockLanguage.setWebResource("com.atlassian.plugins.myplugin:mywebresource");

        when(languageParser.parseRegisteredLanguage(Mockito.<StringReader>any(), anyString())).thenReturn(mockLanguage);

        return mockLanguage;
    }

    private PluginArtifact getMockPlugin(RegisteredLanguage mockLanguage) throws IOException {
        PluginArtifact mockPlugin = Mockito.mock(PluginArtifact.class);
        when(pluginGenerator.createPluginForLanguage(eq(mockLanguage), any(StringReader.class))).thenReturn(mockPlugin);
        return mockPlugin;
    }
}
