package com.atlassian.confluence.ext.code.languages.installers;

import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.atlassian.confluence.ext.code.languages.impl.BuiltinLanguage;
import com.atlassian.confluence.ext.code.languages.impl.LanguageDescriptorFacadeMock;
import com.atlassian.confluence.ext.code.languages.impl.RhinoLanguageParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Unit tests for {@link BuiltInLanguageInstaller}
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class BuiltInLanguageInstallerTestCase {
    @Mock
    private LanguageRegistry languageRegistry;

    LanguageDescriptorFacadeMock descriptorFacadeMock;
    private BuiltInLanguageInstaller installer;

    @Before
    public void setUp() throws Exception {
        descriptorFacadeMock = new LanguageDescriptorFacadeMock();
        MockitoAnnotations.initMocks(this);
        // Note: using the _real_ LanguageParser implementation since it's pretty isolated, has no dependencies on Confluence and is much easier than trying to create a useful mocked implementation.
        installer = new BuiltInLanguageInstaller(descriptorFacadeMock.descriptorFacade, new RhinoLanguageParser(), languageRegistry);
    }

    /**
     * Kinda just tests that it works.
     */
    @Test
    public void testInstallLanguages() throws Exception {
        installer.onStart();

        int numBrushes = descriptorFacadeMock.descriptorFacade.listBuiltinBrushes().length;
        verify(languageRegistry, times(numBrushes)).addLanguage(any(BuiltinLanguage.class));
    }
}
