package com.atlassian.confluence.ext.code.themes.impl;

import com.atlassian.confluence.ext.code.themes.Theme;
import com.atlassian.confluence.ext.code.themes.UnknownThemeException;
import junit.framework.TestCase;

import java.io.InputStream;

/**
 * Unit test-case for the {@link ThemeRegistryImpl} and the {@link ThemeParser}.
 */
public final class ThemeRegistryImplTestCase extends TestCase {

    private ThemeDescriptorFacadeMock facadeMock;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setUp() throws Exception {
        facadeMock = new ThemeDescriptorFacadeMock();
    }

    /**
     * Tests whether the built-in languages XML can be successfully parsed.
     *
     * @throws Exception In case of test errors
     */
    public void testParsing() throws Exception {
        ThemeRegistryImpl registry = new ThemeRegistryImpl(facadeMock.getMock());
        mockBuiltinLoading(registry);

        assertTrue(registry.isThemeRegistered("Default"));
    }

    /**
     * Tests for the behaviour when trying to access unknown themes.
     *
     * @throws Exception In case of test errors
     */
    public void testUnknownTheme() throws Exception {
        ThemeRegistryImpl registry = new ThemeRegistryImpl(facadeMock.getMock());
        mockBuiltinLoading(registry);

        assertFalse(registry.isThemeRegistered("UNKNOWN"));
        String result = null;
        try {
            result = registry.getWebResourceForTheme("UNKNOWN");
            fail();
        } catch (UnknownThemeException e) {
            assertNull(result);
        }
    }

    /**
     * Checks whether for each of the builtin theme the correct script file
     * exists.
     *
     * @throws Exception In case of test errors
     */
    public void testBuiltinPresent() throws Exception {
        ThemeRegistryImpl registry = new ThemeRegistryImpl(facadeMock.getMock());

        mockBuiltinLoading(registry);

        for (Theme theme : registry.listThemes()) {
            assertNotNull(theme.getName());

            if (theme.isBuiltIn()) {
                InputStream is = null;
                try {
                    is = getClass().getClassLoader().getResourceAsStream(
                            theme.getStyleSheetUrl());
                    assertNotNull("No theme file found for theme "
                            + theme.getName(), is);
                } finally {
                    if (is != null) {
                        is.close();
                    }
                }
            }
        }
    }

    /**
     * Setup mocking of built-in theme loading.
     *
     * @param registry The theme registry to mock loading for
     * @throws Exception In case of loading errors
     */
    private void mockBuiltinLoading(final ThemeRegistryImpl registry) throws Exception {
        registry.registerDefaultThemes();
    }

}
