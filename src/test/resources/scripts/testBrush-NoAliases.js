/**
 * A Test SyntaxHighligher custom brush that forgets to declare any aliases for the brush.
 **/

function Brush() {

}

Brush.prototype = new SyntaxHighlighter.Highlighter();

SyntaxHighlighter.brushes.MyTest = Brush;